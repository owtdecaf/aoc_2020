#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main()
{
    std::vector<int> num_list;
    std::vector<int> ans_list;
    std::ifstream inf("..\\..\\Input\\Day1.txt");

    int num;
    bool found = false;

    while (inf >> num)
    {
        num_list.push_back(num);
    }

    for (int i = 0; (unsigned)i <= (unsigned)num_list.size() - 1; i++) {
        if (found == true) {
            break;
        }

        int current_num = num_list[i];
        int temp_num = 2020 - num_list[i];

        for (int y = 0; (unsigned)y <= (unsigned)num_list.size() - 1; y++) {
            if (temp_num == num_list[y]) {
                int sanity_check = current_num + temp_num;
                std::cout << current_num << " + " << temp_num << " = " << sanity_check << "\n";

                if (sanity_check == 2020) {
                    int answer = current_num * temp_num;
                    std::cout << answer << "\n";
                    found = true;
                    break;
                }
            }
        }
    }
}