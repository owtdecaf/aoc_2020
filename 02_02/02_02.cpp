#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main()
{
    std::vector<std::string> string_list;
    std::ifstream inf("..\\..\\Input\\Day2.txt");
    std::string line;

    int total = 0;

    while (getline(inf, line))
    {
        string_list.push_back(line);
    }

    for (unsigned int i = 0; i < string_list.size(); i++) {
        std::string nums = string_list[i].substr(0, string_list[i].find(" "));

        std::string min = nums.substr(0, nums.find("-"));
        std::string max = nums.substr(min.size() + 1);

        int min_int = std::atoi(min.c_str());
        int max_int = std::atoi(max.c_str());
        size_t num_length = nums.size();

        std::string first_half = string_list[i].substr(0, string_list[i].find(":"));
        //Add 2 to account for ":" and " "
        size_t first_half_length = first_half.size() + 2;
        size_t second_half_length = string_list[i].size() - num_length;

        std::string password = string_list[i].substr(first_half_length, second_half_length);
        char letter = string_list[i].at(num_length + 1);

        bool first_found = false;
        bool second_found = false;

        if (password.size() > min_int) {
            //std::cout << "\nFirst Letter: " << password.at(min_int - 1) << "\n";
            if (password.at(min_int - 1) == letter) {
                first_found = true;
            }
        }

        if (password.size() >= max_int) {
            //std::cout << "Second Letter: " << password.at(max_int - 1) << "\n";
            if (password.at(max_int - 1) == letter) {
                second_found = true;
            }
        }

        //if (first_found == true && second_found == false) {
        //    total++;
        //    std::cout << min_int << " " << max_int << " " << letter << " " << password << " " << password.size() << " 1:True\n";
        //}
        //else if (first_found == false && second_found == true) {
        //    total++;
        //    std::cout << min_int << " " << max_int << " " << letter << " " << password << " " << password.size() << " 2:True\n";
        //}
        //else {
        //    std::cout << min_int << " " << max_int << " " << letter << " " << password << " " << password.size() << " False\n";
        //}
    }

    std::cout << "\n\nTotal: " << total;
}