#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main()
{
    std::vector<int> num_list;
    std::ifstream inf("..\\..\\Input\\Day1.txt");

    int num;
    bool found = false;

    while (inf >> num)
    {
        num_list.push_back(num);
    }

    for (int i = 0; (unsigned)i <= (unsigned)num_list.size() - 1; i++) {
        int current_num_1 = num_list[i];
        int temp_num_1 = 2020 - num_list[i];

        if (found == true) {
            break;
        }

        for (int y = 0; (unsigned)y <= (unsigned)num_list.size() - 1; y++) {
            if (found == true) {
                break;
            }

            int current_num_2 = num_list[y];
            int temp_num_2 = temp_num_1 - current_num_2;

            for (int z = 0; (unsigned)z <= (unsigned)num_list.size() - 1; z++) {
                int current_num_3 = num_list[z];

                if (temp_num_2 == num_list[z]) {
                    int sanity_check = current_num_1 + current_num_2 + current_num_3;
                    std::cout << current_num_1 << " + " << current_num_2 << " + " << current_num_3 << " = " << sanity_check << "\n";

                    if (sanity_check == 2020) {
                        int answer = current_num_1 * current_num_2 * current_num_3;
                        std::cout << answer << "\n";
                        found = true;
                        break;
                    }
                }
            }
        }
    }
}